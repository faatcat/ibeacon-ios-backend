<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gcm
 *
 * @author Cryos
 */
class GCM {

    //Google cloud messaging GCM-API url
    private static $url = 'https://android.googleapis.com/gcm/send';
    public $pushStatus = "GCM Status Message will appear here";

    public static function sendMessageThroughGCM($registration_id, $title, $description, $url) {
        $fields = array(
            'to' => (string)$registration_id,
            'notification' => array(
                "body" => $description,
                "title" => $title,
                "icon" => "myicon.png"
            ),
            'data' => array("title" => $title, "description" => $description, "pictureUrl" => $url),
        );
        // Update your Google Cloud Messaging API Key
        define("GOOGLE_API_KEY", "AIzaSyCEFe0mJ3BXL67f7emzt9QC_jPXDtRTJJ8"); //INSERT API_SERVER_KEY HERE
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, GCM::$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

?>