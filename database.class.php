<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of database
 *
 * @author Cryos
 */
class Database {

    private static $dbName = 'beacondb';
    private static $dbHost = '127.0.0.1';
    private static $dbUsername = '';
    private static $dbUserPassword = '';
    public static $cont = null;

    private static function connect() {
        if (null == self::$cont) {
            try {
                self::$cont = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$cont;
    }

    public function __construct() {
        self::connect();
    }

    public function __destruct() {
        //self::disconnect();
    }

    public static function disconnect() {
        self::$cont = null;
    }

    public static function getDatabaseHandler() {
        return self::$cont;
    }

    public static function getDrivers() {
        return PDO::getAvailableDrivers();
    }

}

?>
