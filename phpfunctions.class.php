<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of phpfunctions
 *
 * @author Cryos
 */
class phpfunctions {

    static function get_web_page($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_MAXREDIRS => 3, // stop after 10 redirects
            CURLOPT_ENCODING => "", // handle compressed
            //CURLOPT_USERAGENT => "test", // name of client
            CURLOPT_AUTOREFERER => true, // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // time-out on connect
            CURLOPT_TIMEOUT => 120, // time-out on response
            CURLOPT_HTTPHEADER => array('Content-type: text/plain', 'Content-length: 100', 'X-Mashape-Key: dQAtmR5QdHmshMApow309Dakd1B3p1B6FfIjsnnPzFNiluxRos')
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);

        curl_close($ch);

        return $content;
    }

}
