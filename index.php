
<?php

error_reporting(E_ALL);

include_once 'database.class.php';
include_once 'beacon.class.php';
include_once 'notification.class.php';
include_once 'gcm.class.php';
include_once 'log.class.php';

//TEST
//$beacon = new Beacon("B9407F30-F5F8-466E-AFF9-25556B57FE6D", 43942, 16440);
//var_dump(Handler::handleRequest($beacon, 4));
//TEST
if ($_SERVER['REQUEST_METHOD'] == 'GET') { //POST fields are case-sensitive!
    if (isset($_GET['beaconUUID']) && isset($_GET['beaconMajor']) && isset($_GET['beaconMinor']) && isset($_GET['registrationId'])) {
        $uuid = filter_input(INPUT_GET, 'beaconUUID', FILTER_SANITIZE_STRING);
        $major = filter_input(INPUT_GET, 'beaconMajor', FILTER_SANITIZE_NUMBER_INT);
        $minor = filter_input(INPUT_GET, 'beaconMinor', FILTER_SANITIZE_NUMBER_INT);
        $registrationId = filter_input(INPUT_GET, 'registrationId', FILTER_SANITIZE_STRING);

        $beacon = new Beacon($uuid, $major, $minor);
        $result = Handler::handleRequest($beacon, $registrationId);
        if ($result != -1) {
            echo json_encode($result);
        } else {
            echo json_encode("Beacon: $uuid not found in database.");
        }
    }

    if (isset($_GET['getDBDrivers'])) {
        print_r(Database::getDrivers);
    } 
    if (isset($_GET['getBeacons'])) {
        foreach (Beacon::getBeacons() as $beacon) {
            echo "$beacon->UUID||$beacon->major||$beacon->minor||";
        }
    }
}

class Handler {

    public static function handleRequest(Beacon $beacon, $registrationId) {
        $beaconId = $beacon->getId();
        if ($beaconId == -1) {
            //beacon does not exist in database
            //log down
            $log = new Log("Nonexisting Beacon", "Device $registrationId notices beacon $beacon->UUID - $beacon->major - $beacon->minor");
            $log->insertIntoDB();
            //register beacon into db?
            return -1;
        } else {
            //BEACON HANDLING
            //check if user is first time encounter beacon
            $isFirstTime = !$beacon->userEncounteredBefore($registrationId);
            //log user entry
            $log = new Log("EnterRegion", "$registrationId entered region of beacon($beacon->id) $beacon->UUID - $beacon->major - $beacon->minor");
            $log->insertIntoDB();

            //if user is not first timer then display notification
            if ($isFirstTime) {
                $result = "First timer!";
                //log down
                $log = new Log("FirstTime", "$registrationId and beacon($beacon->id) $beacon->UUID - $beacon->major - $beacon->minor");
                $log->insertIntoDB();
            } else {
                //retrive relevant notification
                $notification = Notification::getNotificationForBeacon($beaconId);
                //notify GCM
                $result = GCM::sendMessageThroughGCM($registrationId, $notification->title, $notification->description, $notification->pictureUrl);
                //log down
                $log = new Log("SentGCM", "Title:$notification->title Desc:$notification->description URL:$notification->pictureUrl");
                $log->insertIntoDB();
            }
            return $result;
        }
    }

}

?>