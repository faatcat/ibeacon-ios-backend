<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of log
 *
 * @author Cryos
 */
class Log {

    public $time;
    public $message;
    public $message2;

    public function __construct($_tag, $_message, $_time = "0") {
        $this->message = (string) $_tag;
        $this->message2 = (string) $_message;
        $this->time = (string) $_time;
    }

    function insertIntoDB() {
        //insert log into database
        $DB = new Database();
        $DBH = $DB->getDatabaseHandler();
        $stmt = $DBH->prepare("INSERT INTO logs (time, message, message2) VALUES (NOW(),?,?);");
        $stmt->bindParam(1, $this->message);
        $stmt->bindParam(2, $this->message2);
        if (!$stmt->execute()) {
            return false;
        };
        return true;
    }

    static function insertLog($tag, $message) {
        (new Log($tag, $message))->insertIntoDB();
    }

    static function viewLogs() {
        $DB = new Database();
        $DBH = $DB->getDatabaseHandler();
        $statement = <<<SQL
    SELECT *
    FROM `logs`
    ORDER BY 'time'
SQL;
        $logs = array();
        $PDOresult = $DBH->query($statement);
        $PDOresult->setFetchMode(PDO::FETCH_ASSOC);

        foreach ($PDOresult->FetchAll() as $row) {
            $log = new Log($row['message'], $row['message2'], $row['time']);
            array_push($logs, $log);
        }
        $DB->disconnect();

        return $logs;
    }

    static function getLogWithTag($tag) {
        $DB = new Database();
        $DBH = $DB->getDatabaseHandler();
        $logs = array();
        $stmt = $DBH->prepare('SELECT *
    FROM logs
    WHERE message = ?');
        $logs = array();
        $result = $stmt->execute(array($tag));
        if ($result) {
            while ($row = $stmt->fetch()) {
                $log = new Log($row['message'], $row['message2'], $row['time']);
                array_push($logs, $log);
            }
        } else {
            //query error
            return $stmt->errorInfo();
        }

        $DB->disconnect();
        return $logs;
    }

}
