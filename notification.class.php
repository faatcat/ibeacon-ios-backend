<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of notification
 *
 * @author Cryos
 */
class Notification {

    public $id;
    public $title;
    public $description;
    public $pictureId;
    public $pictureUrl;

    public function __construct($title, $description, $pictureUrl = -1) {
        $this->title = $title;
        $this->description = $description;
        $this->pictureUrl = $pictureUrl;
    }

    public static function getNotificationForBeacon($beaconId) {
        $DB = new Database();
        $DBH = $DB->getDatabaseHandler();
        $statement = <<<SQL
        SELECT i.url, n.title, n.description 
        FROM notifications n, images i 
        WHERE n.beacon_id = :beaconId AND i.notification_id = n.id
        LIMIT 1
SQL;
        $stmt = $DBH->prepare($statement);
        $stmt->bindParam(":beaconId",$beaconId);
        if ($stmt->execute()) {
            //execute successfully
        } else {
            //failed
            return -1;
        }
        if ($row = $stmt->fetch()) {
            return new Notification($row['title'],$row['description'],$row['url']);
        } else {
            return -1;
            //dun have any notifications for that beacon
        }
    }

}
