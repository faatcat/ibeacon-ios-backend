<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of beacon
 *
 * @author Cryos
 */

class Beacon {

    public $id;
    public $UUID;
    public $major;
    public $minor;

    public function __construct($_UUID, $_major, $_minor, $_id = -1) {
        $this->id = (int) $_id;
        $this->UUID = (string) $_UUID;
        $this->major = (int) $_major;
        $this->minor = (int) $_minor;
    }

    
    function equals(Beacon $beacon) {
        if ($this->UUID == $beacon->UUID && $this->major == $beacon->major && $this->minor == $beacon->minor) {
            return true;
        } else {
            return false;
        }
    }

    function userEncounteredBefore($deviceId) {
        $logs = Log::getLogWithTag("EnterRegion");
        foreach ($logs as $log) {
            //1 entered region of beacon(1) B9407F30-F5F8-466E-AFF9-25556B57FE6D - 43942 - 16440
            if ($log->message2 == "$deviceId entered region of beacon($this->id) $this->UUID - $this->major - $this->minor") {
                return true;
            }
        }
        return false;
    }

    function registerBeacon() {
        //register Beacon into database
        $DB = new Database();
        $DBH = $DB->getDatabaseHandler();
        $stmt = $DBH->prepare("INSERT INTO beacons (UUID, major, minor) VALUES (?,?,?);");
        $stmt->bindParam(1, $this->UUID);
        $stmt->bindParam(2, $this->major);
        $stmt->bindParam(3, $this->minor);
        if (!$stmt->execute()) {
            return false;
        };

        //attach notification to new beacon
        $stmt = $DBH->prepare("INSERT INTO notifications (beacon_id, title, description) VALUES ((SELECT id FROM beacons WHERE UUID=? AND major=? AND minor=?),?,?) ;");
        $stmt->bindParam(1, $this->UUID);
        $stmt->bindParam(2, $this->major);
        $stmt->bindParam(3, $this->minor);
        $stmt->bindParam(4, "Notification");
        $stmt->bindParam(5, "Notification from beacon major: " . $this->major . ", minor: " . $this->minor);
        if (!$stmt->execute()) {
            return false;
        };

        return true;
    }

    static function getBeacons() {
        $DB = new Database();
        $DBH = $DB->getDatabaseHandler();
        $statement = <<<SQL
    SELECT *
    FROM `beacons`
SQL;
        $beacons = array();
        $PDOresult = $DBH->query($statement);
        $PDOresult->setFetchMode(PDO::FETCH_ASSOC);

        foreach ($PDOresult->FetchAll() as $row) {
            $beacon = new Beacon($row['UUID'], $row['major'], $row['minor'], $row['id']);
            array_push($beacons, $beacon);
        }
        $DB->disconnect();

        return $beacons;
    }

    static function exists(Beacon $beacon) {
        return (queryId($beacon) != -1);
    }

    function getId() {
        if ($this->id == -1) {
            $this->id = self::queryId($this);
        }
        return $this->id;
    }

    static function queryId(Beacon $beacon) {
        $beacons = self::getBeacons();
        foreach ($beacons as $b) {
            if ($b->equals($beacon)) {
                return $b->id;
            }
        }
        return -1;
    }

}
